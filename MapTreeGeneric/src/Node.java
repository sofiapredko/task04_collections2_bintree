class Node {
    Node left;
    Node right;
    int value;

    public Node() {
        left = null;
        right = null;
        this.value = value;
    }

    public Node(int n) {
        left = null;
        right = null;
        this.value = n;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node n) {
        left = n;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node n) {
        right = n;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int d) {
        value = d;
    }
}
