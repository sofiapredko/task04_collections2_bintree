public class JTree<T extends Comparable<T>> {

    private JNode<T> root;

    public void insert(T data) {
        root = insertNode(root, data);
    }

    public JNode<T> insertNode(JNode<T> root, T data) {
        if (root == null) {
            return new JNode<T>(data);
        } else if (data.compareTo(root.data) < 0) {
            root.left = insertNode(root.left, data);
        } else if (data.compareTo(root.data) > 0) {
            root.right = insertNode(root.right, data);
        }
        return root;
    }

    public void delete(T data) {
        root = deleteNode(root, data);
    }

    private JNode<T> deleteNode(JNode<T> root, T data) {
        if (root == null) {
            return null;
        } else if (data.compareTo(root.data) < 0) {
            root.left = deleteNode(root.left, data);
        } else if (data.compareTo(root.data) > 0) {
            root.right = deleteNode(root.right, data);
        } else {

            if (root.left == null && root.right == null) {
                return null;
            } else if (root.right == null) {
                return root.left;
            } else if (root.left == null) {
                return root.right;
            } else {
                root.data = findMax(root.left);
                root.left = deleteNode(root.left, root.data);
            }
        }

        return root;
    }

    private T findMax(JNode<T> root) {
        while (root.right != null) {
            root = root.right;
        }
        return root.data;
    }

    public void printInorder()
    {
        inorder(root);
        System.out.println();
    }

    private void inorder(JNode<T> root)
    {
        if (root == null)
            return;

        inorder(root.left);
        System.out.print(" " + root.data);
        inorder(root.right);
    }
}
