public class JNode<T> {
    T data;
    JNode<T> left, right;
    public JNode(T data) {
        this.data = data;
    }
}