public class Main {
    public static void main(String[] args) {
        OrdinaryTree tree1 = new OrdinaryTree();
        tree1.insert(50);
        tree1.insert(30);
        tree1.insert(20);
        tree1.insert(40);
        tree1.insert(70);
        tree1.insert(60);
        tree1.insert(80);
        System.out.println("Ordinary int binary tree");
        tree1.inorderPrint();
        tree1.deleteNode(20);
        System.out.println();
        tree1.inorderPrint();
        System.out.println();

        JTree<Integer> tree2 = new JTree<>();

        for (int i = 0; i < 5; i++)
        {
            int r = (int)(Math.random() * 100) + 1;
            //System.out.println("Inserting " + r + "...");
            tree2.insert(r);
        }

        System.out.println("Generic Integer BT:");

        tree2.printInorder();
        tree2.insert(13);
        tree2.printInorder();


        JTree<String> strtree = new JTree<>();
        strtree.insert("hello");
        strtree.insert("1999");
        strtree.insert("Sofia");
        strtree.insert("Lviv");

        System.out.print("Generic String BT:   ");
        strtree.printInorder();

        Double[] a = new Double[]{0.03, 13.9, 99.0, 55.678, 37.3};
        JTree<Double> doutree = new JTree<>();
        for(Double n : a) doutree.insert(n);
        System.out.print("Generic Double BT:  ");

        doutree.printInorder();
        System.out.println();

    }
}
