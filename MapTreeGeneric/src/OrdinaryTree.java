public class OrdinaryTree {

    private Node root;

    public OrdinaryTree() {
        root = null;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public void insert(int value) {
        root = insert(root, value);
    }

    private Node insert(Node currentnode, int value) {
        if (currentnode == null)
            currentnode = new Node(value);

        if (value < currentnode.value) {
            currentnode.left = insert(currentnode.left, value);
        } else if (value > currentnode.value) {
            currentnode.right = insert(currentnode.right, value);
        } else {
            return currentnode;
        }
        return currentnode;
    }

    public void deleteNode(int value) {
        root = deleteRecNode(root, value);
    }

    private Node deleteRecNode(Node root, int value) {
        if (root == null) return root;

        if (value < root.value)
            root.left = deleteRecNode(root.left, value);
        else if (value > root.value)
            root.right = deleteRecNode(root.right, value);

        else {
            if (root.left == null)
                return root.right;
            else if (root.right == null)
                return root.left;

            root.value = minValue(root.right);
            root.right = deleteRecNode(root.right, root.value);
        }
        return root;
    }

    private int minValue(Node root) {
        int minvalue = root.value;
        while (root.left != null) {
            minvalue = root.left.value;
            root = root.left;
        }
        return minvalue;
    }

    public void inorderPrint() {
        inorderRecPrint(root);
    }

    private void inorderRecPrint(Node root) {
        if (root != null) {
            inorderRecPrint(root.left);
            System.out.print(root.value + " ");
            inorderRecPrint(root.right);
        }
    }

}




