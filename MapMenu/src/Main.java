import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String keyMenu;
        View file = new View();
        while (true) {
            file.showMenu();
            System.out.println("Please select menu point.");
            keyMenu = scan.nextLine();
            System.out.println("\n\n");
            try {
                file.methodsmenu.get(keyMenu).start();
                System.out.println();
            } catch (Exception e) {
                if (keyMenu.equals("0")) {
                    break;
                } else {
                    System.out.println("Error, please try again.\n");
                }
                e.printStackTrace();
            }
        }
    }

}
