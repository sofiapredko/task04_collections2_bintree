import java.util.LinkedHashMap;
import java.util.Map;

public class View {

    public Map<String, String> menu;
    public Map<String, Functional> methodsmenu;

    public View() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsmenu = new LinkedHashMap<>();

        menu.put("1)" , FuncList.PRINT.name());
        menu.put("2)", FuncList.OFFER.name());
        menu.put("3)", FuncList.INFO.name());
        menu.put("4)", FuncList.EXIT.name());

        methodsmenu.put("1", this::printS);
        methodsmenu.put("2", this::offer);
        methodsmenu.put("3", this::getInfo);
        methodsmenu.put("4", this::exit);
    }

    private void printS(){
        System.out.println(FuncList.PRINT.getName());
        System.out.println();
        System.out.println("I am printing");
    }

    private void offer(){
        System.out.println(FuncList.OFFER.getName());
        System.out.println();
        System.out.println("Okey, I will do my best");
    }

    private void getInfo(){
        System.out.println(FuncList.INFO.getName());
        System.out.println();
        System.out.println("Okey, here is your info");
        System.out.println(java.time.LocalDate.now());
        System.out.println(java.time.LocalTime.now());
    }

    private void exit(){
        System.out.println(FuncList.EXIT.getName());
        System.out.println();
        System.out.println("You`ve chosen to exit. Bye! See you soon");
        System.exit(0);
    }


    public void showMenu() {
        for (String obj : menu.values()) {
            System.out.println(obj);
        }
    }

}