public enum FuncList {

    PRINT ("Wanna print something?"),
    OFFER("How can I help you?"),
    EXIT ("Exit now"),
    INFO ("Getting info");

    private final String name;

    FuncList(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
